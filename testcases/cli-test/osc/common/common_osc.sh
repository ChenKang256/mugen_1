#!/usr/bin/bash

# Copyright (c) 2021. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author        :   zhujinlong
#@Contact       :   zhujinlong@163.com
#@Date          :   2020-11-2
#@License       :   Mulan PSL v2
#@Desc          :   Public class
#####################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function deploy_env() {
    DNF_INSTALL osc
    mkdir -p /root/.config/osc/
    cat >/root/.config/osc/oscrc <<EOF
[general]
apiurl=https://api.opensuse.org

[https://api.opensuse.org]
user=test666
pass=test666@123
credentials_mgr_class=osc.credentials.TransientCredentialsManager
EOF
    branches_path=home:test666
    currentDir=$(
        cd "$(dirname "$0")" || exit 1
        pwd
    )
    expect <<EOF
        spawn osc list
        expect {
            "Password*" {
                send "test666@123\\r"
            }
        }
        expect eof
EOF

}

function clear_env() {
    osc up
    cd "$currentDir" || exit 1
    rm -rf "$branches_path" /root/.config/osc
    DNF_REMOVE "$@"
}
