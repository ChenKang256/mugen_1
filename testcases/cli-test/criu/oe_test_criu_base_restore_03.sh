#!/usr/bin/bash

# Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# ##################################
# @Author    :   huangjiayi
# @Contact   :   1960887351@qq.com
# @Date      :   2023/7/22
# @License   :   Mulan PSL v2
# @Desc      :   Test "criu" command
# ##################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test(){
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "criu"
    mkdir checkpoint_file
    test_process(){
        num=0
        while true;
        do
            echo -e "$num PID, BASHPID, 和PPID是$$, $BASHPID, $PPID"
            (( num += 1 ))
            sleep 1
        done
    }
    DUMP(){
        sleep 0.1
        criu dump -j -t "$pid"
    }
    LOG_INFO "End to prepare the test environment."
}

function run_test(){
    LOG_INFO "Start to run test."
    cd checkpoint_file || exit
    (test_process) & pid=$! && DUMP
    criu restore -j --link-remap & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore --link-remap failed"
    criu restore -j --ghost-limit size & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore --ghost-limit failed"
    criu restore -j -l & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore -l failed"
    criu restore -j --file-locks & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore --file-locks failed"
    criu restore -j -L / & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore -L failed"
    criu restore -j --libdir / & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore --libdir failed"
    criu restore -j --irmap-scan-path FILE & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore --irmap-scan-path failed"
    criu restore -j --manage-cgroups & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore --manage-cgroups failed"
    criu restore -j --cgroup-root / & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore --cgroup-root failed"
    criu restore -j --cgroup-props STRING & DUMP
    CHECK_RESULT $? 0 0 "Check criu restore --cgroup-props failed"
    cd .. || exit
    LOG_INFO "End to run test."
}

function post_test(){
    LOG_INFO "Start to restore the test environment."
    DNF_REMOVE "$@"
    rm -rf checkpoint_file
    LOG_INFO "End to restore the test environment."
}

main "$@"