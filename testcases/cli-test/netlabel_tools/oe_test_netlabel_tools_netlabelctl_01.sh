#!/usr/bin/bash
# Copyright (c) 2022. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   huike
# @Contact   :   754873891@qq.com
# @Date      :   2022/11/20
# @License   :   Mulan PSL v2
# @Desc      :   verify the uasge of nasm command
# ############################################

source ${OET_PATH}/libs/locallibs/common_lib.sh
function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL netlabel_tools
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test"
    netlabelctl -h 2>&1 | grep 'NetLabel Control Utility'
    CHECK_RESULT $? 0 0 "Check netlabelctl -h failed"
    netlabelctl -V 2>&1 | grep 'NetLabel Control Utility'
    CHECK_RESULT $? 0 0 "Check netlabelctl -V failed"
    netlabelctl -p map list 2>&1 | grep 'Configured NetLabel'
    CHECK_RESULT $? 0 0 "Check netlabelctl -p map list failed"
    netlabelctl calipso add pass doi:11
    CHECK_RESULT $? 0 0 "Check netlabelctl add pass doi failed"
    netlabelctl -p unlbl accept on
    CHECK_RESULT $? 0 0 "Check netlabelctl -p unlbl accept on failed"
    netlabelctl unlbl accept off
    CHECK_RESULT $? 0 0 "Check netlabelctl unlbl accept off failed"
    netlabelctl unlbl list 2>&1 | grep 'accept'
    CHECK_RESULT $? 0 0 "Check netlabelctl unlbl list failed"
    netlabelctl -v mgmt version 2>&1 | grep '[[:digit:]]'
    CHECK_RESULT $? 0 0 "Check netlabelctl -v mgmt version failed"
    netlabelctl -v mgmt protocols 2>&1 | grep 'UNLABELED,CIPSOv4,CALIPSO'
    CHECK_RESULT $? 0 0 "Check netlabelctl -v mgmt protocols failed"
    netlabelctl mgmt version 2>&1 | grep '[[:digit:]]'
    CHECK_RESULT $? 0 0 "Check netlabelctl mgmt version failed"
    LOG_INFO "End to run test"
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    netlabelctl calipso del doi:11
    DNF_REMOVE
    LOG_INFO "Een to restore the test environment."
}

main $@
