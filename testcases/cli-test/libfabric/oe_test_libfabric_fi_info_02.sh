#!/usr/bin/bash
# Copyright (c) 2022. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   huike
# @Contact   :   754873891@qq.com
# @Date      :   2022/11/20
# @License   :   Mulan PSL v2
# @Desc      :   verify the uasge of libfabric command
# ############################################

source "${OET_PATH}/libs/locallibs/common_lib.sh"

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL libfabric
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    fi_info -t'FI_EP_MSG' 2>&1 | grep 'provider: sockets'
    CHECK_RESULT $? 0 0 "Check fi_info -t failed"
    fi_info --ep_type='FI_EP_MSG' 2>&1 | grep 'provider: sockets'
    CHECK_RESULT $? 0 0 "Check fi_info --ep_type failed"
    fi_info -f'127.0.0.1/32' 2>&1 | grep -i 'provider: UDP'
    CHECK_RESULT $? 0 0 "Check fi_info -f failed"
    fi_info --fabric='127.0.0.1/32' 2>&1 | grep -i 'provider: UDP'
    CHECK_RESULT $? 0 0 "Check fi_info --fabric failed"
    fi_info -a'FI_FORMAT_UNSPEC' 2>&1 | grep -i 'provider: UDP'
    CHECK_RESULT $? 0 0 "Check fi_info -a failed"
    fi_info --addr_format='FI_FORMAT_UNSPEC' 2>&1 | grep -i 'provider: UDP'
    CHECK_RESULT $? 0 0 "Check fi_info --addr_format failed"
    fi_info -e 2>&1 | grep '# FI_LOG_LEVEL: String'
    CHECK_RESULT $? 0 0 "Check fi_info -e failed"
    fi_info --env 2>&1 | grep '# FI_LOG_LEVEL: String'
    CHECK_RESULT $? 0 0 "Check fi_info --env failed"
    fi_info -l 2>&1 | grep 'usnic:'
    CHECK_RESULT $? 0 0 "Check fi_info -l failed"
    fi_info --list 2>&1 | grep 'usnic:'
    CHECK_RESULT $? 0 0 "Check fi_info --list failed"
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"
