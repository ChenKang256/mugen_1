#!/usr/bin/bash

# Copyright (c) 2022. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   liujingjing
# @Contact   :   liujingjing25812@163.com
# @Date      :   2021/01/11
# @License   :   Mulan PSL v2
# @Desc      :   The usage of commands in docker package
# ############################################
# shellcheck disable=SC1091

source "../common/common_podman.sh"
function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    deploy_env
    podman pull ubi8-minimal
    podman run --name postgres -e POSTGRES_PASSWORD=secret -d ubi8-minimal
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    docker image history --format=json ubi8-minimal | grep "comment"
    CHECK_RESULT $?
    docker image history --human ubi8-minimal | grep "B"
    CHECK_RESULT $?
    docker image history --no-trunc ubi8-minimal | grep "$(docker images -aq)"
    CHECK_RESULT $?
    docker image history -q ubi8-minimal | grep "$(docker images -aq)"
    CHECK_RESULT $?
    docker image ls --filter after=ubi8-minimal
    CHECK_RESULT $?
    docker image ls --all | grep "ubi8-minimal"
    CHECK_RESULT $?
    docker image ls --digests | grep "DIGEST"
    CHECK_RESULT $?
    docker image ls --format json | grep "ubi8-minimal"
    CHECK_RESULT $?
    docker image ls --no-trunc | grep "sha256"
    CHECK_RESULT $?
    docker image ls --noheading | grep "TAG"
    CHECK_RESULT $? 1
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    clear_env
    LOG_INFO "End to restore the test environment."
}

main "$@"
