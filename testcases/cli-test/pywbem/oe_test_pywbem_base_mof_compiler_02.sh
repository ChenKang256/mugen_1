#!/usr/bin/bash

# Copyright (c) 2022. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author    	:   LifanHan
#@Contact   	:   514362169@qq.com
#@Date      	:   2022-08-02 10:44:52
#@License   	:   Mulan PSL v2
#@Version   	:   1.0
#@Desc      	:   Test pywbem_mof_compiler command
#####################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    cp -f ./common/CIM_Application26.mof ./
    DNF_INSTALL "docker python3-pywbem"
    sudo docker pull hub.firefly.store/kschopmeyer/openpegasus-server:0.1.1
    sudo docker run -d -it --rm -p 0.0.0.0:15988:5988 -p 0.0.0.0:15989:5989 --name openpegasus hub.firefly.store/kschopmeyer/openpegasus-server:0.1.1
    SLEEP_WAIT 10s
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    if [ "${NODE1_FRAME}" == x86_64 ]; then
        mof_compiler -s http://0.0.0.0:15988 CIM_Application26.mof -I ./
        CHECK_RESULT $? 0 0 "mof_compiler: failed to test option -I"
        mof_compiler -s http://0.0.0.0:15988 -v CIM_Application26.mof | grep "Compiling file"
        CHECK_RESULT $? 0 0 "mof_compiler: failed to test option -v"
        mof_compiler -V | grep "$(rpm -qa python3-pywbem | awk -F "-" '{print $3}')"
        CHECK_RESULT $? 0 0 "mof_compiler: failed to test option -V"
        mof_compiler -h | grep "usage"
        CHECK_RESULT $? 0 0 "mof_compiler: failed to test option -h"
    fi
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    docker stop openpegasus
    docker rm -f openpegasus
    rm -rf CIM_Application26.mof
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"
