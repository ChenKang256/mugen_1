#!/usr/bin/bash

# Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   liuyafei1
# @Contact   :   chenyia@uniontech.com
# @Date      :   2023-4-03
# @License   :   Mulan PSL v2
# @Desc      :   Command test-md5sum 
# ############################################

source "${OET_PATH}/libs/locallibs/common_lib.sh"

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    echo "123456" >testfile1
    CHECK_RESULT $? 0 0 "check create file fail"
    md5sum  testfile1  > testfile1.md5  
    test -f testfile1.md5
    CHECK_RESULT $? 0 0 "The testfile1.md5 is not exit "  
    md5sum -c testfile1.md5
    CHECK_RESULT $? 0 0 "md5sum check failed"
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "start environment cleanup."
    rm -rf testfile1 testfile1.md5
    LOG_INFO "Finish environment cleanup!"
}

main "$@"



