#!/usr/bin/bash

# Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   aliceye666
# @Contact   :   yezhifen@uniontech.com
# @Date      :   2024-1-23
# @License   :   Mulan PSL v2
# @Desc      :   command arp
# ############################################
source "${OET_PATH}"/libs/locallibs/common_lib.sh

function run_test() {
    LOG_INFO "start to run test."
    arp -h
    CHECK_RESULT $? 0 0 "show arp help fail"
    arp -a | grep gateway
    CHECK_RESULT $? 0 0 "display (all) hosts in alternative (BSD) style fail"
    arp -n | grep -i address
    CHECK_RESULT $? 0 0 "don't resolve names fail"
    arp -v | grep Entries
    CHECK_RESULT $? 0 0 "be verbose fail"
    LOG_INFO "End to run test."
}

main "$@"
