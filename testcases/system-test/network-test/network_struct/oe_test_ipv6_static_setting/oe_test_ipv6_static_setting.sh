#!/usr/bin/bash

# Copyright (c) 2024. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author    	:   wangdi
#@Contact   	:   15710801006@163.com
#@Date      	:   2023-10-25
#@License   	:   Mulan PSL v2
#@Desc      	:   Set static ipv6
#####################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL net-tools
    test_nic=$(TEST_NIC 1 | awk '{print $1}')
    cp /etc/sysconfig/network /etc/sysconfig/network.bak
    cp /etc/sysconfig/network-scripts/ifcfg-"${NODE1_NIC}" /etc/sysconfig/network-scripts/ifcfg-"${test_nic}"
    sed -i "s/${NODE1_NIC}/${test_nic}/g" /etc/sysconfig/network-scripts/ifcfg-"${test_nic}"
    sed -i "s/UUID/#UUID/g" /etc/sysconfig/network-scripts/ifcfg-"${test_nic}"
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    systemctl restart NetworkManager
    CHECK_RESULT $? 0 0 "Restart network failed."
    SLEEP_WAIT 5
    ifconfig -a "$test_nic" | grep "inet6"
    CHECK_RESULT $? 0 0 "Check ipv6 failed."
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    nmcli con down "$test_nic"
    rm -rf /etc/sysconfig/network-scripts/ifcfg-"${test_nic}"
    mv -f /etc/sysconfig/network.bak /etc/sysconfig/network
    systemctl restart NetworkManager
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"
