#!/usr/bin/bash

# Copyright (c) 2024. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author        :   linqian
#@Contact       :   1179362010@qq.com
#@Date          :   2024-06-12
#@License       :   Mulan PSL v2
#@Desc          :   oeAware test
#####################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "oeAware-manager"
    \cp -rf /etc/oeAware/config.yaml /etc/oeAware/config_ori.yaml
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    echo > /var/log/oeAware/server.log
    sed -i '/enable_list/a\ - name: libpmua.so' /etc/oeAware/config.yaml
    systemctl restart oeaware
    SLEEP_WAIT 1
    oeawarectl --query libpmua.so | grep "Plugin query failed, because plugin does not exist" && grep "plugin libpmua.so cannot be enabled, because it does not exist" /var/log/oeAware/server.log
    CHECK_RESULT $? 0 0 "plugin query failed"
    echo > /var/log/oeAware/server.log
    \cp -rf /etc/oeAware/config_ori.yaml /etc/oeAware/config.yaml
    sed -i '/enable_list/a\ - name:' /etc/oeAware/config.yaml
    systemctl restart oeaware
    SLEEP_WAIT 1
    grep "plugin null cannot be enabled, because it does not exist" /var/log/oeAware/server.log
    CHECK_RESULT $? 0 0 "plugin query failed"
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    sed -i '/libpmua.so/d' /etc/oeAware/config.yaml
    systemctl restart oeaware
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"
