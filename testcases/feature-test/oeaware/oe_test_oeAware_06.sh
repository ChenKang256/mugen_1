#!/usr/bin/bash

# Copyright (c) 2024. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author        :   linqian
#@Contact       :   1179362010@qq.com
#@Date          :   2024-06-11
#@License       :   Mulan PSL v2
#@Desc          :   oeAware test
#####################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "oeAware-manager"
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    \cp -rf /etc/oeAware/config.yaml /etc/oeAware/config_ori.yaml
    log_level_new='1'
    sed -i -e '/^log_level: / s/: .*/: '${log_level_new}'/' /etc/oeAware/config.yaml
    grep -F "log_level: 1" /etc/oeAware/config.yaml
    CHECK_RESULT $? 0 0 "modify config file failed"
    systemctl restart oeaware
    log_path=$(awk -F ":" '/log_path/{print $2}' /etc/oeAware/config.yaml | awk '{$1=$1};1')
    SLEEP_WAIT 5
    grep -F "log path: ""${log_path}"", log level: 1" "${log_path}"/server.log
    CHECK_RESULT $? 0 0 "log_path modify failed"

    log_level_new='2'
    sed -i -e '/^log_level: / s/: .*/: '${log_level_new}'/' /etc/oeAware/config.yaml
    grep -F "log_level: 2" /etc/oeAware/config.yaml
    CHECK_RESULT $? 0 0 "modify config file failed"
    systemctl restart oeaware
    log_path=$(awk -F ":" '/log_path/{print $2}' /etc/oeAware/config.yaml | awk '{$1=$1};1')
    SLEEP_WAIT 5
    grep -F "log path: ""${log_path}"", log level: 2" "${log_path}"/server.log
    CHECK_RESULT $? 0 0 "log_path modify failed"

    log_level_new='3'
    sed -i -e '/^log_level: / s/: .*/: '${log_level_new}'/' /etc/oeAware/config.yaml
    grep -F "log_level: 3" /etc/oeAware/config.yaml
    CHECK_RESULT $? 0 0 "modify config file failed"
    systemctl restart oeaware
    log_path=$(awk -F ":" '/log_path/{print $2}' /etc/oeAware/config.yaml | awk '{$1=$1};1')
    SLEEP_WAIT 5
    grep -F "log path: ""${log_path}"", log level: 3" "${log_path}"/server.log
    CHECK_RESULT $? 0 0 "log_path modify failed"

    log_level_new='4'
    sed -i -e '/^log_level: / s/: .*/: '${log_level_new}'/' /etc/oeAware/config.yaml
    grep -F "log_level: 4" /etc/oeAware/config.yaml
    CHECK_RESULT $? 0 0 "modify config file failed"
    systemctl restart oeaware
    log_path=$(awk -F ":" '/log_path/{print $2}' /etc/oeAware/config.yaml | awk '{$1=$1};1')
    SLEEP_WAIT 5
    grep -F "log path: ""${log_path}"", log level: 4" "${log_path}"/server.log
    CHECK_RESULT $? 0 0 "log_path modify failed"
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    mv /etc/oeAware/config_ori.yaml /etc/oeAware/config.yaml
    systemctl restart oeaware
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"
